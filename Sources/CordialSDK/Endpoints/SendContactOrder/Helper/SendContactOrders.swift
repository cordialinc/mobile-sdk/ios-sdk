//
//  SendContactOrders.swift
//  CordialSDK
//
//  Created by Yan Malinovsky on 4/23/19.
//  Copyright © 2019 cordial.io. All rights reserved.
//

import Foundation

class SendContactOrders {
    
    let cordialAPI = CordialAPI()
    let internalCordialAPI = InternalCordialAPI()
    
    let requestSender = DependencyConfiguration.shared.requestSender
    
    func sendContactOrders(sendContactOrderRequests: [SendContactOrderRequest]) {
        if let url = URL(string: CordialApiEndpoints().getOrdersURL()) {
            var request = CordialRequestFactory().getCordialURLRequest(url: url, httpMethod: .POST)
            
            let sendContactOrderJSON = getSendContactOrderRequestsJSON(sendContactOrderRequests: sendContactOrderRequests)
            request.httpBody = sendContactOrderJSON.data(using: .utf8)
            
            let downloadTask = CordialURLSession.shared.backgroundURLSession.downloadTask(with: request)
            
            let sendContactOrdersURLSessionData = SendContactOrdersURLSessionData(sendContactOrderRequests: sendContactOrderRequests)
            let cordialURLSessionData = CordialURLSessionData(taskName: API.DOWNLOAD_TASK_NAME_SEND_CONTACT_ORDERS, taskData: sendContactOrdersURLSessionData)
            CordialURLSession.shared.setOperation(taskIdentifier: downloadTask.taskIdentifier, data: cordialURLSessionData)
            
            self.requestSender.sendRequest(task: downloadTask)
        }
    }
    
    private func getSendContactOrderRequestsJSON(sendContactOrderRequests: [SendContactOrderRequest]) -> String {
        var sendContactOrdersArrayJSON = [String]()
        
        sendContactOrderRequests.forEach { sendContactOrderRequest in
            if let sendContactOrderJSON = self.getSendContactOrderRequestJSON(sendContactOrderRequest: sendContactOrderRequest) {
                sendContactOrdersArrayJSON.append(sendContactOrderJSON)
            } else {
                LoggerManager.shared.error(message: "Failed to create order JSON. Error: [Some order data are invalid.] ", category: "CordialSDKSendContactOrders")
            }
        }
        
        let sendContactOrdersStringJSON = sendContactOrdersArrayJSON.joined(separator: ", ")
        
        let sendContactOrderJSON = "[ \(sendContactOrdersStringJSON) ]"
        
        return sendContactOrderJSON
    }

    internal func getSendContactOrderRequestJSON(sendContactOrderRequest: SendContactOrderRequest) -> String? {
        if let orderJSON = self.getOrderJSON(order: sendContactOrderRequest.order) {
            var rootContainer  = [
                "\"deviceId\": \"\(self.internalCordialAPI.getDeviceIdentifier())\"",
                "\"order\": \(orderJSON)"
            ]

            if let primaryKey = self.cordialAPI.getContactPrimaryKey(),
               let escapedPrimaryKey = self.internalCordialAPI.escapeStringForJSON(primaryKey) {

                rootContainer.append("\"primaryKey\": \(escapedPrimaryKey)")
            }

            if let mcID = sendContactOrderRequest.mcID, let mcTapTime = self.internalCordialAPI.getCurrentMcTapTime() {
                rootContainer.append("\"mcID\": \"\(mcID)\"")
                rootContainer.append("\"mcTapTime\": \"\(mcTapTime)\"")
            }

            let rootContainerString = rootContainer.joined(separator: ", ")

            return "{ \(rootContainerString) }"
        }

        return nil
    }

    private func getOrderJSON(order: Order) -> String? {
        if let escapedOrderID = self.internalCordialAPI.escapeStringForJSON(order.orderID),
           let escapedStatus = self.internalCordialAPI.escapeStringForJSON(order.status),
           let escapedStoreID = self.internalCordialAPI.escapeStringForJSON(order.storeID),
           let escapedCustomerID = self.internalCordialAPI.escapeStringForJSON(order.customerID),
           let escapedShippingAddress = self.getAddressJSON(address: order.shippingAddress),
           let escapedBillingAddress = self.getAddressJSON(address: order.billingAddress) {

            var orderContainer  = [
                "\"orderID\": \(escapedOrderID)",
                "\"status\": \(escapedStatus)",
                "\"storeID\": \(escapedStoreID)",
                "\"customerID\": \(escapedCustomerID)",
                "\"purchaseDate\": \"\(CordialDateFormatter().getTimestampFromDate(date: order.purchaseDate))\"",
                "\"shippingAddress\": \(escapedShippingAddress)",
                "\"billingAddress\": \(escapedBillingAddress)",
                "\"items\": \(UpsertContactCart().getCartItemsJSON(cartItems: order.items))"
            ]

            if let tax = order.tax {
                orderContainer.append("\"tax\": \"\(tax)\"")
            }

            if let shippingAndHandling = order.shippingAndHandling {
                orderContainer.append("\"shippingAndHandling\": \(shippingAndHandling)")
            }

            if let properties = order.properties {
                orderContainer.append("\"properties\": \(API.getDictionaryJSON(properties))")
            }

            let orderContainerString = orderContainer.joined(separator: ", ")

            return "{ \(orderContainerString) }"
        }

        return nil
    }

    private func getAddressJSON(address: Address) -> String? {
        if let escapedName = self.internalCordialAPI.escapeStringForJSON(address.name),
           let escapedAddress = self.internalCordialAPI.escapeStringForJSON(address.address),
           let escapedCity = self.internalCordialAPI.escapeStringForJSON(address.city),
           let escapedState = self.internalCordialAPI.escapeStringForJSON(address.state),
           let escapedPostalCode = self.internalCordialAPI.escapeStringForJSON(address.postalCode),
           let escapedCountry = self.internalCordialAPI.escapeStringForJSON(address.country) {

            let addressContainer  = [
                "\"name\": \(escapedName)",
                "\"address\": \(escapedAddress)",
                "\"city\": \(escapedCity)",
                "\"state\": \(escapedState)",
                "\"postalCode\": \(escapedPostalCode)",
                "\"country\": \(escapedCountry)"
            ]

            let addressContainerString = addressContainer.joined(separator: ", ")

            return "{ \(addressContainerString) }"
        }

        return nil
    }
}
